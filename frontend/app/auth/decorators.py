from flask import g, flash, redirect, url_for
from functools import wraps


def login_required(view):
    """Wrapper for views that require an user to be logged in.

    Prevents access to the wrapped view if the user is not logged in.
    """
    @wraps(view)
    def wrapped(**kwargs):
        if g.current_user is None:
            flash('You need to be logged in to access this page.', 'info')
            return redirect(url_for('auth.login.login'))

        return view(**kwargs)

    return wrapped
