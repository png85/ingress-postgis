from flask import (Blueprint, session, redirect, request,
                   flash, redirect, url_for, render_template)
from werkzeug.security import check_password_hash

from sqlalchemy.orm.exc import NoResultFound

from app import app
from app import v
from app.auth import registration_allowed
from app.rbac.models import User

from hashlib import md5

import datetime
import random
import string

from flask_wtf import FlaskForm
from wtforms.fields.core import StringField
from wtforms.fields.simple import PasswordField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    """Form specs for login form."""
    username = StringField('Username',
                           validators=[DataRequired()])
    password = PasswordField('password',
                             validators=[DataRequired()])


bp = Blueprint('login', __name__, url_prefix='/login')


def __login_allowed(user):
    """Check if user is allowed to login

    Performs various checks on the given user account and lists reasons why the user should
    be rejected from logging in.

    Args:
        user (User): User object that is trying to login

    Returns:
        [list]: List of strings containing reasons why the login should be rejected.
    """
    reasons = []
    if user.banned:
        reasons.append(f'User {user.username} is banned')

    if not user.allow_password_login:
        reasons.append(
            f'Password login is disabled for {user.username}')

    if not user.password_hash or len(user.password_hash) == 0:
        reasons.append(f'{user.username} has no passwort set.')

    return reasons


def __generate_v_auth_state():
    """Generate random state string for V OAuth authentication

    Returns:
        string: Random state for V authentication
    """
    s = [random.choice(string.ascii_letters) for _ in range(16)]
    return ''.join(s).encode('utf-8')


@bp.route('/login.html', methods=['POST', 'GET'])
def login():
    DEFAULT_REDIRECT = url_for('auth.login.login')
    form = LoginForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                with app.DB.session.begin() as tx:
                    user = User.query.where(User.username == form.username.data) \
                        .one()

                    reject_reasons = __login_allowed(user)
                    if len(reject_reasons) != 0:
                        for r in reject_reasons:
                            flash(r, 'danger')
                        return redirect(DEFAULT_REDIRECT)

                    if not check_password_hash(user.password_hash, form.password.data):
                        flash('Invalid username/password supplied!', 'danger')
                        return redirect(DEFAULT_REDIRECT)

                    session.clear()
                    session['user_id'] = user.id

                    user.last_login = datetime.datetime.now()

                    flash(f'User <em>{user.username}</em> logged in.',
                          'success')
                    return redirect(request.url_root)

            except NoResultFound:
                flash(f'Invalid username/password supplied!', 'danger')

    vauth = v.Authenticator()
    vauth.set_redirect_url(__v_auth_url())
    vauth.set_scopes([v.SCOPE_PROFILE])
    vauth.client_id = app.config['V_API_CLIENT_ID']
    vauth.client_secret = app.config['V_API_CLIENT_SECRET']
    vauth.state = __generate_v_auth_state()
    session['vauth_state'] = md5(vauth.state).hexdigest()

    return render_template('auth/login.html',
                           register_allowed=registration_allowed(),
                           v_auth_url=vauth.get_auth_url(),
                           login_form=form
                           )


def __v_auth_url():
    return '{}{}'.format(request.host_url.rstrip('/'),
                         url_for('v.authenticate'))


@bp.route('/logout.html', methods=['GET'])
def logout():
    """Log out existing session

    Destroys the user's session and redirects to the landing page.
    """
    session.clear()
    return redirect(request.url_root)
