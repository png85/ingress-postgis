from flask import (Blueprint, redirect, request, flash,
                   abort, url_for, render_template)
from flask_wtf.form import FlaskForm

import sqlalchemy
from werkzeug.security import generate_password_hash
from wtforms.fields.core import StringField
from wtforms.fields.simple import PasswordField
from wtforms.validators import DataRequired

from app import app
from app.rbac.models import User
from app.rbac import username_exists
from app.auth import registration_allowed
bp = Blueprint('register', __name__, url_prefix='/register')


class RegistrationForm(FlaskForm):
    username = StringField(label='Username', validators=[DataRequired()])
    password = PasswordField(label='Password', validators=[DataRequired()])


@bp.route('/register.html', methods=['POST', 'GET'])
def register():
    """Registration for new password-based accounts.

    When called as a GET request, this displays a form to sign up a new user.
    For POST requests, it will try to create a new User instance from the
    supplied form data.
    """
    if not registration_allowed():
        abort(403)

    form = RegistrationForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                username = form.username.data
                password = form.password.data

                with app.DB.session.begin() as tx:
                    # Make sure the username doesn't exist
                    if username_exists(username):
                        flash(f"User <em>{username}</em> already exists!")
                        return redirect(url_for('auth.login.login'))

                    # Create new User instance
                    user = User(username=username,
                                password_hash=generate_password_hash(password),
                                allow_password_login=True,
                                banned=False,
                                v_verified=False,
                                v_enlid=None,
                                v_points=None,
                                v_level=None)
                    app.DB.session.add(user)

                    flash(f'User <em>{username}</em> registered successfully.',
                          'success')
                    return redirect(url_for('auth.login.login'))

            except Exception as e:
                flash(f'Failed to create new User object: {str(e)}')

    return render_template('auth/register.html',
                           registration_form=form)
