from flask import Blueprint, json, jsonify, request

from .. import password_policy

bp = Blueprint('passwords', __name__, url_prefix='/pwd')
ajax_bp = Blueprint('ajax', __name__, url_prefix='/ajax')


@ajax_bp.route('/strength.json', methods=['POST'])
def strength():
    if not 'q' in request.form or len(request.form['q']) == 0:
        return jsonify({'success': False,
                        'message': 'Missing query string.'})

    try:
        p = password_policy.password(request.form['q'])
        failed_tests = [t.name() for t in p.test()]

        return jsonify({'success': True,
                        'strength': p.strength(),
                        'entropy_bits': p.entropy_bits,
                        'valid': (len(failed_tests) == 0),
                        'failed_tests': failed_tests})

    except Exception as e:
        return jsonify({'success': False,
                        'message': f'Caught exception: {str(e)}'})


@ajax_bp.route('/suggest.json')
def suggest_password():
    from app.auth import password_policy
    from app.diceware import generate_passphrase

    phrase = generate_passphrase()
    p = password_policy.password(phrase)

    return jsonify({
        'suggestion': phrase,
        'entropy_bits': p.entropy_bits,
        'strength': p.strength()
    })


bp.register_blueprint(ajax_bp)
