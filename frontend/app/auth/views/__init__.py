from flask import (Blueprint, session, g, abort)

from . import login
from . import register
from . import passwords

from app.rbac.models import User
from app import app

bp = Blueprint('auth', __name__, url_prefix='/auth')

bp.register_blueprint(login.bp)
bp.register_blueprint(register.bp)
bp.register_blueprint(passwords.bp)


@bp.before_app_request
def load_user_data():
    """Load user data for currently logged in user.
    """
    try:
        user_id = session['user_id']

        if user_id is None:
            g.current_user = None
        else:
            with app.DB.session.begin() as tx:
                user = User.query.where(User.id == user_id).one()

                if user.banned:
                    abort(403)

                g.current_user = user.to_dict()

    except KeyError:
        g.current_user = None
