from app import app
from password_strength.policy import PasswordPolicy

password_policy = PasswordPolicy.from_names(length=8,
                                            entropybits=30,
                                            strength=0.5)


def registration_allowed():
    """Check if registration of new accounts is enabled.

    Returns:
        bool: True if new accounts can be registered, otherwise False
    """
    if not 'RBAC_REGISTRATION_ALLOWED' in app.config:
        return False

    if not isinstance(app.config['RBAC_REGISTRATION_ALLOWED'], bool):
        return False

    return app.config['RBAC_REGISTRATION_ALLOWED']


if True:
    from . import decorators
    from . import views
