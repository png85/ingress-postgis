import logging
from app import app
import os
import requests


class Wordlist:
    """Wrapper class for diceware wordlist

    Raises:
        OSError: If the cache directory for the downloaded wordlist doesn't exist
        IOError: On I/O errors while reading the wordlist file
    """
    WORDLIST = {
        'url': 'https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt',
        'file': 'eff_large_wordlist.txt'
    }

    DOWNLOAD_CHUNK_SIZE = 8192

    def _download_wordlist(self):
        try:
            with requests.get(self.WORDLIST['url'], stream=True) as r:
                r.raise_for_status()
                bytes_written = 0
                with open(self.wordlist_file, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=self.DOWNLOAD_CHUNK_SIZE):
                        bytes_written += f.write(chunk)

                app.logger.debug('Downloaded %d bytes of wordlist data.',
                                 bytes_written)

        except Exception as e:
            app.logger.error('Failed to download wordlist: %s', str(e))
            return False

        return True

    def _load_wordlist(self):
        if not os.path.exists(self.wordlist_file):
            app.logger.info(
                'Wordlist not found in cache. Trying to download...')
            if not self._download_wordlist():
                return False
            app.logger.info('Wordlist download finished successfully.')

        try:
            app.logger.debug('Reading wordlist from %s', self.wordlist_file)
            self.words = {}
            with open(self.wordlist_file) as f:
                count = 0
                for line in f:
                    (pattern, word) = line.split()
                    self.words[pattern] = word
                    count += 1
                app.logger.debug('Read %d words from %s',
                                 count, self.wordlist_file)

        except IOError as e:
            app.logger.error('Failed to load wordlist data from %s: %s',
                             self.wordlist_file, str(e))
            return False

        return True

    def __init__(self,
                 cache_dir=os.path.join(app.instance_path, 'cache')) -> None:

        if not os.path.exists(cache_dir):
            raise OSError(f"Cache directory {cache_dir} doesn't exist!")

        self.wordlist_file = os.path.join(cache_dir, self.WORDLIST['file'])

        if not self._load_wordlist():
            raise IOError('Failed to load wordlist file.')
