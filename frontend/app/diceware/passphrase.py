from .wordlist import Wordlist
import secrets

DICE_COUNT = 5


def generate_passphrase(wordlist=None, words=5, separator='-'):
    if wordlist is None:
        wordlist = Wordlist()

    rolls = []
    for _ in range(words):
        dice = ''.join(str(secrets.randbelow(6) + 1)
                       for _ in range(DICE_COUNT))
        rolls.append(dice)

    word_list = []
    for i in rolls:
        for (k, v) in wordlist.words.items():
            if i == k:
                word_list.append(v)

    return separator.join(word_list)
