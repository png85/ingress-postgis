// ==UserScript==
// @author         png
// @name           IITC plugin: Ingress-GIS
// @category       Portal Info
// @version        0.0.1
// @description    Bridge to Ingress-GIS Web Application
// @id             debug-raw-portal-data
// @match          https://intel.ingress.com/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    // use own namespace for plugin
    window.plugin.gis = function() {};

    window.plugin.gis.setupCallback = function() {
        addHook('portalDetailsUpdated', window.plugin.gis.addPushLink);
    }

    window.plugin.gis.addPushLink = function() {
        $('.linkdetails').append(`<aside>`
        +`<a href="#" onclick="window.plugin.gis.pushPortal('${window.selectedPortal}');" title="GIS: Import Portal">`
        +`<i class="bi-file-earmark-plus"></i>`
        +`</a>`
        +`</aside>`
            );
    }

    window.plugin.gis.pushPortal = function(guid) {
        if (!window.portals[guid]) {
            console.warn('Invalid GUID: ' + guid);
            return;
        }

        console.log('Selected GUID to push: ' + guid); 

        var data = window.portals[guid].options.data;
        var portal_json = JSON.stringify(data);

        $.ajax({
            type: "PUT",
            url: "http://localhost:5000/api/portals/" + guid,
            contentType: 'application/json',
            data: portal_json,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            success: function(result) {
                if (!result.success) {
                    console.error('[GIS] Failed to push portal: ' + result.messsage);
                    return;
                }

                console.log('[GIS] Pushed portal: ' + result.guid + ', is_new: ' + result.is_new);
            }
        });
    }

    window.plugin.gis.isVisible = function(p) {
        b = window.map.getBounds();

        if (p._latlng.lat < b._southWest.lat ||
            p._latlng.lng < b._southWest.lng ||
            p._latlng.lat > b._northEast.lat ||
            p._latlng.lng > b._northEast.lng) {
                return false;
            }

        return true;
    }

    window.plugin.gis.bulkImport = function() {
        var portals = {};
        for (var guid in window.portals) {
            var portal = window.portals[guid];
            if (window.plugin.gis.isVisible(portal)) {
                portals[guid] = portal.options.data;
            }
        }

        if (Object.keys(portals).length == 0) {
            return;
        }
        
        console.log(`Uploading ${Object.keys(portals).length} portals.`)
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:5000/api/portals/bulk',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(portals),
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            success: function(result) {
                if (result.guids.failed.length > 0) {
                    result.guids.failed.forEach(function (guid) {
                        var message = result.results[guid].message;
                        console.log(`[GIS/Bulk] Update failed for GUID ${guid}: ${message}`);
                    });
                }
            }
        });
    }

    var setup = function() {
        var biStyleLink = document.createElement('link');
        biStyleLink.rel = 'stylesheet';
        biStyleLink.href = 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css';
        document.getElementsByTagName('head')[0].appendChild(biStyleLink);

        window.plugin.gis.setupCallback();

        $('#toolbox').append(
            `<a href="#" onclick="window.plugin.gis.bulkImport();" title="GIS: Bulk Import"><i class="bi-file-earmark-arrow-up"></i></a>`
        );
    }
    
    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
    } // wrapper end

    // inject code into site context
    var script = document.createElement('script');
    var info = {};
    if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
    script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
    (document.body || document.head || document.documentElement).appendChild(script);
    
    