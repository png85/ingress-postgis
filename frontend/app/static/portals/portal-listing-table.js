const renderPortalIDs = function (data, type, row) {
    if (type == 'display') {
        return '<span title="GUID: ' + row.ingress_guid + '">' + data + '</span>';
    }

    return data;
};

const renderPortalImage = function (data, type) {
    if (type == 'display') {
        return '<img src="' + data + '" class="img-fluid rounded"  style="max-width: 64px; max-height: 64px;" />';
    }

    return data;
};

const renderIntelLink = function (data, type) {
    if (type == 'display') {
        return '<a href="' + data + '" target="_new" class="link-primary" title="Open Intel Map"><i class="bi-geo-alt"></i></a>';
    }

    return data;
}

const renderLocation = function (data, type, row) {
    if (type == 'display') {
        geoJson = wellknown.parse(data);

        return geoJson.coordinates[1].toFixed(6) + '<br/>' + geoJson.coordinates[0].toFixed(6)
    }

    return data;
}

const renderActions = function (data, type, row) {
    if (type == 'display') {
        var $detailLink = `<a href="${row.id}.html"><i class="bi-info-circle-fill"></i></a>`;
        var deleteLink = '<a href="#" class="link-danger" data-bs-toggle="modal" data-bs-target="#modal-delete-' + row.id + '" title="Delete"><i class="bi-trash"></i></a>';

        return $detailLink + ' ' + deleteLink;
    }
    
    return '';
};
