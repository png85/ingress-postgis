/**
 * Toggle `banned` flag for an user
 *
 * Performs an AJAX request to toggle the `banned` flag for the given `user_id`.
 *
 * @param {int} user_id Unique ID of the user whose `banned` flag shall be toggled.
 */
function userList_toggleBannedFlag(user_id)
{
    $.get(Flask.url_for('rbac.user.ajax.toggle_banned', { 'user_id' : user_id }), function(data, textStatus, jqXHR) {
        if (data.success) {
            var e           = document.getElementById(`ban-state-${user_id}`);
            const className = data.banned ? 'fa fa-lock text-danger' : 'fa fa-lock-open text-success';

            e.setAttribute('class', className);
        } else {
            alert(`Failed to toggle ban state for User ID ${user_id}: ${data.message}`);
        }
    }, 'json');
}

/**
 * Toggle `allow_password_login` flag for an user
 *
 * Performs an AJAX request to toggle the `allow_password_login` flag for the given `user_id`.
 *
 * @param {int} user_id Unique of the user whose `allow_password_login` flag shall be toggled.
 */
function userList_togglePasswordLoginFlag(user_id)
{
    $.get(Flask.url_for('rbac.user.ajax.toggle_password_login', { 'user_id' : user_id }),
          function(data, textStatus, jqXHR) {
              if (data.success) {
                  var e           = document.getElementById(`allow-password-login-${user_id}-icon`);
                  const className = data.allow_password_login ? 'fa fa-check-circle text-success'
                                                              : 'fa fa-times-circle text-danger';

                  e.setAttribute('class', className);
              } else {
                  alert(`Failed to toggle password login for User ID ${user_id}: ${data.message}`);
              }
          },
          'json');
}

function userList_revoke_role(user_id, role_id)
{
    const ajaxURL = Flask.url_for('rbac.user.ajax.revoke_role', {
        'user_id' : user_id,
        'role_id' : role_id,
    });

    $.ajax({
        url : ajaxURL,
        method : 'GET',
        dataType : 'json',
        success : function(data, textStatus, jqXHR) {
            if (data.success) {
                var roleTable = $('#modal-roles-table').DataTable();
                roleTable.clear();
                roleTable.rows.add(data.roles);
                roleTable.draw();

                var userTable = $('#userList').DataTable();
                userTable.ajax.reload();
            } else {
                alert(`Failed to revoke role from user: ${data.message}`);
            }
        },
    });
}

/**
 * Display grantable flag in role listing
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_modal_render_grantable(data, type, row)
{
    if (type == 'display') {
        return data ? `<i class="fa fa-check-circle"></i>` : `<i class="fa fa-cross-circle"></i>`;
    }

    return data;
}

/**
 * Display action buttons in role listing
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_modal_render_actions(data, type, row)
{
    if (type == 'display') {
        const user_id  = $('#modal-roles-user_id').val();
        var revokeLink = document.createElement('a');

        revokeLink.setAttribute('href', '#');
        revokeLink.setAttribute('class', 'link-danger');
        revokeLink.setAttribute('onclick', `userList_revoke_role(${user_id}, ${row.id});`);
        revokeLink.setAttribute('title', `Revoke role "${row.name}"`);

        revokeLink.innerHTML = `<i class="fa fa-minus-circle"></i>`;

        return revokeLink.outerHTML;
    }

    return '';
}

function userList_show_roles(user_id, username, roles_json)
{
    //
    // Set user data in header
    $('#modal-roles-username').html(username);
    $('#modal-roles-user_id').val(user_id);

    //
    // Update role table
    roles = JSON.parse(roles_json);
    if ($.fn.dataTable.isDataTable('#modal-roles-table')) {
        // already have a DataTable, just reset it
        var table = $('#modal-roles-table').DataTable();
        table.clear();
        table.rows.add(roles);
        table.draw();
    } else {
        // need to create new DataTable
        var table = $('#modal-roles-table').DataTable({
            data : roles,
            paging : false,
            ordering : false,
            searching : false,
            columns : [
                {
                    data : 'name', // Role name
                },
                {
                    data : 'grantable',                       // Grantable flag
                    className : 'text-center',                //
                    render : userList_modal_render_grantable, //
                },
                {
                    className : 'text-center',              // Action buttons
                    render : userList_modal_render_actions, //
                }
            ],
            order : [ [ 0, 'asc' ] ],
            bInfo : false,
        });
    }
}

/**
 * Display username
 *
 * Renders a link to an users profile.
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_render_username(data, type, row)
{
    if (type == 'display') {
        const url = Flask.url_for('rbac.user.profile', { 'user_id' : row.id })
        var e     = document.createElement('a');

        e.setAttribute('href', url);
        e.setAttribute('class', 'link-info text-decoration-none');
        e.innerText = row.username;

        return e.outerHTML;
    }

    return data;
}

/**
 * Display role count
 *
 * Renders a button containing the number roles an user currently has.
 * Clicking the button will toggle a modal showing a detailed role listing.
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_render_roles(data, type, row)
{
    if (type == 'display') {
        var e            = document.createElement('button');
        const roles_json = JSON.stringify(row.roles);

        e.setAttribute('id', `roles-${row.id}`);
        e.setAttribute('onclick', `userList_show_roles(${row.id}, '${row.username}', '${roles_json}');`);
        e.setAttribute('data-bs-target', '#modal-roles');
        e.setAttribute('data-bs-toggle', 'modal');
        e.setAttribute('class', 'btn btn-info btn-sm');
        e.innerHTML = data.length;

        return e.outerHTML;
    }
    return data;
}

/**
 * Display account ban state
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_render_banned(data, type, row)
{
    if (type == 'display') {
        var btn = document.createElement('button');
        btn.setAttribute('class', 'btn btn-light btn-sm')
        btn.setAttribute('onclick', `userList_toggleBannedFlag(${row.id});`);
        btn.setAttribute('title', 'Click to toggle');

        var className = data ? 'fa fa-lock text-danger' : 'fa fa-lock-open text-success';
        var e         = document.createElement('i');
        e.setAttribute('class', className);
        e.setAttribute('id', `ban-state-${row.id}`);

        btn.appendChild(e);

        return btn.outerHTML;
    }
    return data;
}

/**
 * Display password login state
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_render_allow_password_login(data, type, row)
{
    if (type == 'display') {
        var btn = document.createElement('button');
        for (let [attr, val] of Object.entries({
                 'id' : `allow-password-login-${row.id}`,
                 'class' : 'btn btn-sm btn-light',
                 'onclick' : `userList_togglePasswordLoginFlag(${row.id});`,
             })) {
            btn.setAttribute(attr, val);
        }

        const imgClassName = data ? 'fa fa-check-circle text-success' : 'fa fa-times-circle text-danger';
        var img            = document.createElement('i');
        for (let [attr, val] of Object.entries({
                 'id' : `allow-password-login-${row.id}-icon`,
                 'class' : imgClassName,
             })) {
            img.setAttribute(attr, val);
        }

        btn.appendChild(img);

        return btn.outerHTML;
    }
    return data;
}

/**
 * Display indicator for password hash availability
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for the current row
 * @returns
 */
function userList_render_has_password_hash(data, type, row)
{
    if (type == 'display') {
        return data ? '<i class="fa fa-check-circle text-success"></i>'
                    : '<i class="fa fa-times-circle text-warning"></i>';
    }

    return data;
}

/**
 * Display V verification status
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_render_v_verified(data, type, row)
{
    if (type == 'display') {
        return data ? '<i class="fa fa-check-circle text-success"></i>'
                    : '<i class="fa fa-times-circle text-danger"></i>';
    }
    return data;
}

/**
 * Display V Level
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_render_v_level(data, type, row)
{
    if (type == 'display') {
        return data != null ? data : '<em>n/a</em>';
    }
    return data;
}

/**
 * Display V Points
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function userList_render_v_points(data, type, row)
{
    if (type == 'display') {
        return data != null ? data : '<em>n/a</em>';
    }
    return data;
}

function userList_init()
{
    var dt = $('#userList').DataTable({
        ajax : {
            url : Flask.url_for('rbac.user.ajax.ajax_list'),
            dataSrc : '',
        },
        columns : [
            {
                data : 'id',               // User ID
                orderable : false,         //
                searchable : false,        //
                className : 'text-center', //
            },
            {
                data : 'username',                 // Username
                orderable : true,                  //
                searchable : true,                 //
                render : userList_render_username, //
            },
            {
                data : 'roles',                // Role count
                orderable : false,             //
                searchable : false,            //
                className : 'text-center',     //
                render : userList_render_roles //
            },
            {
                data : 'allow_password_login',                // Password login flag
                orderable : false,                            //
                searchable : false,                           //
                className : 'text-center',                    //
                render : userList_render_allow_password_login //
            },
            {
                data : 'has_password_hash',                 // Password hash flag
                orderable : false,                          //
                searchable : false,                         //
                className : 'text-center',                  //
                render : userList_render_has_password_hash, //
            },
            {
                data : 'banned',                // Banned flag
                orderable : false,              //
                searchable : false,             //
                className : 'text-center',      //
                render : userList_render_banned //
            },
            {
                data : 'v_verified',                // V verification state
                orderable : false,                  //
                searchable : false,                 //
                className : 'text-center',          //
                render : userList_render_v_verified //
            },
            {
                data : 'v_level',                 // V Level
                orderable : false,                //
                searchable : false,               //
                className : 'text-center',        //
                render : userList_render_v_level, //
            },
            {
                data : 'v_points',                // V Points
                orderable : false,                //
                searchable : false,               //
                className : 'text-center',        //
                render : userList_render_v_points //
            }
        ],
        order : [ [ 1, 'asc' ] ]
    });
}