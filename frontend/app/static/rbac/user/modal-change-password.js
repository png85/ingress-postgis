function changePasswordRequest()
{
    $.ajax({
        url : Flask.url_for('rbac.user.ajax.change_password'),
        method : 'POST',
        data : {
            pw_1st : $('#pw_1st').val(),
            pw_2nd : $('#pw_2nd').val(),
            pw_old : $('#pw_old').val(),
        },
        dataType : 'json',
        success : function(data, textStatus, jqXHR) {
            if (data.success) {
                const stateElement = document.getElementById('password-state');
                stateElement.setAttribute('class', 'fa fa-check-circle text-success');

                $('#modal-change-password').modal('toggle');
            } else {
                alert(`Failed to update password: ${data.message}`);
            }
        }
    });
}

function changePassword_ajax_success(data, textStatus, jqXHR)
{
    var reasons   = $('#passwordRejectReasons'); // reject reason list
    var progress  = $('#passwordProgress');      // progress bar
    var className = 'bg-info';                   // default CSS class for progress bar

    // clear existing CSS classes and reasons
    ['bg-danger', 'bg-warning', 'bg-success', className].forEach(className => { progress.removeClass(className); });
    reasons.empty();

    if (data.success) {
        // update progress bar
        progress.attr('data-transitiongoal', data.strength * 100).progressbar({
            display_text : 'fill',
            use_percentage : false,
        });

        if (data.valid) {
            if (data.strength < 0.33) {
                className = 'bg-danger';
            } else if (data.strength >= 0.33 && data.strength <= 0.66) {
                className = 'bg-warning';
            } else if (data.strength > 0.66) {
                className = 'bg-success';
            }
        } else {
            const reasonStrings = {
                'length' : 'Password is too short.',
                'entropybits' : 'Password entropy is too low.',
                'strength' : 'Password strength is too low',
            };

            data.failed_tests.forEach(testName => {
                var e = document.createElement('li');
                for (let [attr, val] of Object.entries({
                         'class' : 'text-small text-info',
                     })) {
                    e.setAttribute(attr, val);
                }

                e.innerText = reasonStrings[testName];
                reasons.append(e);
            });
        }
    } else {
        progress.attr('data-transitiongoal', 0).progressbar({
            display_text : 'fill',
            use_percentage : false,
        });

        var msgElement = document.createElement('li');
        for (let [attr, val] of Object.entries({
                 'class' : 'text-danger',
             })) {
            msgElement.setAttribute(attr, val);
        }
        msgElement.innerText = data.message;
        reasons.append(msgElement);
    }

    progress.addClass(className);
}

function changePassword_keyup()
{
    var pw = $('#pw_1st').val();
    if (pw.length > 0) {
        $.ajax({
            url : Flask.url_for('auth.passwords.ajax.strength'),
            method : 'POST',
            data : {
                q : pw,
            },
            dataType : 'json',
            success : changePassword_ajax_success,
        });
    }
}

function changePasswordModal_init()
{
    $('#btnChangePassword').click(changePasswordRequest);
    $('#pw_1st').keyup(changePassword_keyup);
}
