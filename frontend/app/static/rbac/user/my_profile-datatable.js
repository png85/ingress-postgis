function userRoleListing_init(tableData)
{
    var table = $('#userRoleListing').DataTable({
        data : tableData,
        ordering : false,
        searching : false,
        paging : false,
        bInfo : false,
        columns : [
            {
                data : 'name',
            },
            {
                data : 'description',
            }
        ],
        order : [ [ 0, 'asc' ] ],
    });
}
