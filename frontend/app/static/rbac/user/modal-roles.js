/**
 * Handler for "Grant" button in role modal
 *
 * Sends an AJAX request to grant a selected role to an user.
 */
function modalRoles_grant_clicked()
{
    const elemUserID = '#modal-roles-user_id';       // User ID element
    const userID     = $(elemUserID).val();          // User ID
    const elemRoleID = '#modal-roles-grant-role_id'; // Role ID element
    const roleID     = $(elemRoleID).val();          // Role ID
    const ajaxURL    = Flask.url_for('rbac.user.ajax.grant_role', {
        'user_id' : userID,
        'role_id' : roleID,
    });

    $.ajax({
        url : ajaxURL,
        method : 'GET',
        dataType : 'json',
        success : function(data, textStatus, jqXHR) {
            if (data.success) {
                var roleTable = $('#modal-roles-table').DataTable();
                roleTable.clear();
                roleTable.rows.add(data.roles);
                roleTable.draw();

                var userTable = $('#userList').DataTable();
                userTable.ajax.reload();
            } else {
                alert(`Failed to grant role to user: ${data.message}`);
            }
        },
    });
}

/**
 * Pick role from search results
 *
 * Updates hidden input elements with the selected role's data.
 *
 * @param {int} role_id Unique ID of selected role
 * @param {int} role_name Name of selected role
 */
function modalRoles_grant_ajaxPickRole(role_id, role_name)
{
    const elemInput  = '#modal-roles-grant-role_name';        // Text input
    const elemOutput = '#modal-roles-grant-role_id';          // Output element
    const elemResult = '#modal-roles-grant-role_name-result'; // Role search result

    $(elemOutput).val(role_id);
    $(elemInput).val(role_name);
    $(elemResult).hide();
}

/**
 * Process AJAX search results
 *
 * Creates UI elements for all grantable roles returned by the AJAX search request.
 *
 * @param {any} data JSON data returned from server
 * @param {string} textStatus Status message
 * @param {any} jqXHR JQuey XMLHttpRequest object
 */
function modalRoles_grant_ajaxResult(data, textStatus, jqXHR)
{
    var html = '';
    data.forEach(result => {
        var e = document.createElement('a');
        e.setAttribute('class', 'dropdown-item');
        e.setAttribute('href', '#');
        e.setAttribute('role', 'option');
        e.setAttribute('onclick', `modalRoles_grant_ajaxPickRole(${result.id}, '${result.name}');`);
        e.innerHTML = result.name;

        html += e.outerHTML;
    });

    const elemResult = '#modal-roles-grant-role_name-result'; // Role search result
    $(elemResult).html(html).show();
}

/**
 * Input handler for role name search box
 *
 * Performs an AJAX request to look for grantable roles
 */
function modalRoles_grant_keyUpHandler()
{
    const targetUserID = $('#modal-roles-user_id').val();       // Target user
    const elemInput    = '#modal-roles-grant-role_name';        // Text input
    const elemResult   = '#modal-roles-grant-role_name-result'; // Role search result
    const elemOutput   = '#modal-roles-grant-role_id';          // Output element

    $(elemOutput).val(null);

    const queryString = $(elemInput).val();
    if (queryString == '') {
        // query is empty
        $(elemResult).hide();
    } else {
        // perform AJAX request to fetch possible roles
        $.post(Flask.url_for('rbac.role.ajax.grantable_role_search'), {
            q : queryString,
            uid : targetUserID,
        },
               modalRoles_grant_ajaxResult, 'json');
    }
}

function modalRoles_init()
{
    //
    const elemInput = '#modal-roles-grant-role_name'; // Text input

    $(elemInput).keyup(modalRoles_grant_keyUpHandler);
}