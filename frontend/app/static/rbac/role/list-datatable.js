/**
 * Rendering callback for usernames in user dialog
 *
 * This used to render a link to an user's profile in the user list modal
 * dialog.
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for current row
 * @returns Requested cell data
 */
function roleList_modal_render_username(data, type, row)
{
    if (type == 'display') {
        var e = document.createElement('a');
        e.setAttribute('href', Flask.url_for('rbac.user.profile', { 'user_id' : row.id }));
        e.setAttribute('class', 'link-info text-decoration-none');
        e.innerHTML = data;
        return e.outerHTML;
    }

    return data;
}

/**
 * Show user list modal for a given role
 *
 * @param {int} role_id Unique ID of thr role that shall be shown
 * @param {string} role_name Role name as string
 * @param {string} users_json JSON data the use for user listing table
 */
function roleList_show_users(role_id, role_name, users_json)
{
    //
    // Update dialog header
    $('#modal-users-role_name').html(role_name);

    //
    // Update DataTable for user listing table
    users = JSON.parse(users_json);
    if ($.fn.dataTable.isDataTable('#modal-users-table')) {
        // We already have a DataTable, just rebuild its data
        var table = $('#modal-users-table');
        table.clear();
        table.rows.add(users);
        table.draw();
    } else {
        // We need to create a new DataTable
        var table = $('#modal-users-table').DataTable({
            data : users,
            paging : false,
            ordering : true,
            searching : false,
            columns : [
                {
                    data : 'username',
                    orderable : true,
                    searchable : false,
                    render : roleList_modal_render_username,
                },
            ],
            order : [ [ 0, 'asc' ] ],
            bInfo : false,
        });
    }
}

/**
 * Rendering callback for role name in role listing
 *
 * This is used to render the role name for an entry in the role list table.
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for the current row
 * @returns Requested cell data
 */
function roleList_render_name(data, type, row)
{
    if (type == 'display') {
        var e       = document.createElement('span');
        e.innerHTML = data;
        e.setAttribute('title', row.description);

        return e.outerHTML;
    }

    return data;
}

/**
 * Rendering callback for user count in role listing
 *
 * This is used to render a button that shows the user list modal for an
 * entry in the role list table.
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data requested
 * @param {Object} row Full data source for the current row
 * @returns Requested cell data
 */
function roleList_render_users(data, type, row)
{
    if (type == 'display') {
        const users_json = JSON.stringify(row.users);
        var e            = document.createElement('button');

        e.setAttribute('class', 'btn btn-info btn-sm');
        e.setAttribute('data-bs-toggle', 'modal');
        e.setAttribute('data-bs-target', '#modal-users');
        e.setAttribute('onclick', `roleList_show_users(${row.id}, '${row.name}', '${users_json}')`);

        e.innerHTML = data.length;

        return e.outerHTML;
    }

    return data;
}

/**
 * Toggle `grantable` flag for a role
 *
 * Executes an AJAX request to toggle the `grantable` flag for the role
 * with the given `role_id`.
 *
 * @param {int} role_id Unique ID of the role that shall have its `grantable`
 * flag toggled.
 */
function roleList_toggle_grantable(role_id)
{
    const url = Flask.url_for('rbac.role.ajax.toggle_grantable', { 'role_id' : role_id });
    $.get(url, function(data, textStatus, jqXHR) {
        if (data.success) {
            var e           = document.getElementById(`grantable-state-${role_id}`);
            const className = data.grantable ? 'fa fa-check-circle text-success' : 'fa fa-times-circle text-danger';

            e.setAttribute('class', className);
        } else {
            alert(data.message);
        }
    }, 'json');
}

/**
 * Render grantable flag
 *
 * Renders a button that can be used to toggle that `grantable` flag for an
 * entry in the role listing table.
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for the current row
 * @returns Requested cell data
 */
function roleList_render_grantable(data, type, row)
{
    if (type == 'display') {
        var btn = document.createElement('button');
        btn.setAttribute('class', 'btn btn-light btn-sm');
        btn.setAttribute('onclick', `roleList_toggle_grantable(${row.id});`);

        const imgClassName = data ? 'fa fa-check-circle text-success' : 'fa fa-times-circle text-danger';
        var img            = document.createElement('i');
        img.setAttribute('class', imgClassName);
        img.setAttribute('id', `grantable-state-${row.id}`);

        btn.innerHTML = img.outerHTML;

        return btn.outerHTML;
    }

    return data;
}

/**
 * Update confirmation modal for role deletion
 *
 * Updates the confirmation modal for role deletion with the given name and role ID.
 *
 * @param {*} role_id Unique ID of the role that shall be deleted
 * @param {*} role_name Name of the role that shall be deleted
 */
function roleList_show_delete_modal(role_id, role_name)
{
    $('#modal-delete-role-role_name').html(role_name)

    const formAction = Flask.url_for('rbac.role.delete', { 'role_id' : role_id });
    var formElement  = document.getElementById('modal-delete-role-form');
    formElement.setAttribute('action', formAction);
}

/**
 * Render action buttons for a role listing entry
 *
 * @param {any} data Cell data
 * @param {string} type Type of cell data request
 * @param {Object} row Full data source for the current row
 * @returns
 */
function roleList_render_actions(data, type, row)
{
    if (type == 'display') {
        // button to trigger edit modal
        var btnEdit = document.createElement('button');
        btnEdit.setAttribute('class', 'btn btn-primary btn-sm');
        btnEdit.setAttribute('title', 'Edit role data');
        btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

        // button to trigger deletion confirmation modal
        var btnDelete = document.createElement('button')
        btnDelete.setAttribute('class', 'btn btn-danger btn-sm');
        btnDelete.setAttribute('title', `Delete role "${row.name}"`);
        btnDelete.setAttribute('onclick', `roleList_show_delete_modal(${row.id}, '${row.name}');`);
        btnDelete.setAttribute('data-bs-target', '#modal-delete-role');
        btnDelete.setAttribute('data-bs-toggle', 'modal');
        btnDelete.innerHTML = '<i class="fa fa-trash-alt"></i>';

        return `${btnEdit.outerHTML} ${btnDelete.outerHTML}`;
    }

    return '';
}

/**
 * Initialize role listing DataTable
 *
 * Sets up a DataTable that fetches all available roles via an AJAX request
 * and displays options to manipulate them.
 */
function roleList_init()
{
    $('#roleList').DataTable({
        ajax : {
            url : Flask.url_for('rbac.role.ajax.ajax_list'),
            dataSrc : '',
        },
        columns : [
            {
                data : 'name',                 // Role name
                render : roleList_render_name, //
                orderable : true,              //
                searchable : true,             //
            },
            {
                data : 'grantable',                 // Grantable flag
                orderable : false,                  //
                searchable : false,                 //
                render : roleList_render_grantable, //
                className : 'text-center',          //
            },
            {
                data : 'users',                 // User count
                orderable : false,              //
                searchable : false,             //
                render : roleList_render_users, //
                className : 'text-center',      //
            },
            {
                render : roleList_render_actions, // Action buttons
                orderable : false,                //
                searchable : false,               //
                className : 'text-center',        //
            }
        ],
        order : [ [ 0, 'asc' ] ]
    });
}