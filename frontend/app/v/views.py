import datetime
from flask import Blueprint, session, redirect, url_for, flash, request
import sqlalchemy
from app import v, app
from app.rbac.models import User
from sqlalchemy.orm.exc import NoResultFound

bp = Blueprint('v', __name__, url_prefix='/v')


@bp.route('/authenticate')
def authenticate():
    LOGIN_URL = url_for('auth.login.login')
    if not 'vauth_state' in session:
        flash('Missing V OAuth state in session', 'danger')
        return redirect(LOGIN_URL)

    for k in ['code', 'state']:
        if not k in request.args:
            flash(f'Missing parameter: <em>{k}</em>',
                  'danger')
            return redirect(LOGIN_URL)

    if request.args['state'] != session['vauth_state']:
        flash('Invalid V OAuth state in session', 'danger')
        return redirect(LOGIN_URL)

    vauth = v.Authenticator()
    vauth.set_scopes([v.SCOPE_PROFILE])
    vauth.client_id = app.config['V_API_CLIENT_ID']
    vauth.client_secret = app.config['V_API_CLIENT_SECRET']
    vauth.code = request.args['code']

    vauth_url = '{}{}'.format(request.host_url.rstrip('/'),
                              url_for('v.authenticate'))
    vauth.set_redirect_url(vauth_url)

    # Obtain API access token
    access_token = vauth.get_token(request.args['state'])
    if not access_token:
        flash('Failed to obtain V API access token!', 'danger')
        return redirect(LOGIN_URL)

    # Fetch V profile for this user and verify flag states
    v_info = vauth.get_v_info()
    if 'V_PROFILE_REQUIRED_FLAGS' in app.config:
        required_flags = app.config['V_PROFILE_REQUIRED_FLAGS']
        rejected = []
        for key in required_flags:
            (expected, message) = required_flags[key]

            if not key in v_info:
                rejected.append(f'Missing flag <em>{key}</em> in V info.')
                continue

            if v_info[key] != expected:
                rejected.append(message)

        if rejected:
            for reason in rejected:
                flash(f'Acess denied: {reason}', 'danger')
            return redirect(LOGIN_URL)

    try:
        with app.DB.session.begin() as tx:
            user = None

            # Try to find user by ENLID
            try:
                user = User.query.where(User.v_enlid == v_info['enlid']).one()

            except NoResultFound:
                pass

            # Check if we have an unlinked account matching this name
            if user is None:
                try:
                    user = User.query.where(sqlalchemy.and_(User.username == v_info['agent'],
                                                            User.v_enlid == None)).one()
                    user.v_enlid = v_info['enlid']

                except NoResultFound:
                    pass

            # No matching user found, create a new onse if registration is enabled
            if user is None:
                if 'RBAC_REGISTRATION_ALLOWED' in app.config and app.config['RBAC_REGISTRATION_ALLOWED'] is True:
                    user = User(username=v_info['agent'],
                                v_enlid=v_info['enlid'],
                                password_hash=None, allow_password_login=False)
                    app.DB.session.add(user)
                    app.DB.session.flush()
                    app.DB.session.refresh(user)

                else:
                    flash(
                        'Registering new accounts is currently not allowed.', 'warning')
                    return redirect(LOGIN_URL)

            # Update V profile data for this user
            user.v_level = v_info['vlevel']
            user.v_points = v_info['vpoints']
            user.v_verified = v_info['verified']

            user.last_login = datetime.datetime.now()

            # Update user ID in session
            session.clear()
            session['user_id'] = user.id

            flash(f'You have been logged in using <em>v.enl.one</em>', 'success')
            return redirect(request.url_root)

    except Exception as e:
        flash(f'Exception during user lookup: {str(e)}', 'danger')
        return redirect(LOGIN_URL)
