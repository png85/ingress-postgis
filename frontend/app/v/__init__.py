from .authenticator import Authenticator
from . import views

# Endpoints
ENDPOINT_AUTH = 'authorize'
ENDPOINT_TOKEN = 'token'
ENDPOINT_VERIFY = 'verify'

# Scopes
SCOPE_OPENID = 'openid'
SCOPE_EMAIL = 'email'
SCOPE_GOOGLEDATA = 'googledata'
SCOPE_TELEGRAM = 'telegram'
SCOPE_PROFILE = 'profile'
SCOPE_TEAMS = 'teams'

# API Scopes
SCOPE_SEARCH = 'search'
SCOPE_TRUSTENLID = 'trustenlid'
SCOPE_TRUSTGID = 'trustgid'
SCOPE_CONNECTIONENLID = 'connectionenlid'
SCOPE_CONNECTIONGID = 'connectiongid'
SCOPE_BULKINFOENLID = 'bulkinfoenlid'
SCOPE_BULKINFOGID = 'bulkinfogid'
SCOPE_BULKINFOTELEGRAMID = 'bulkinfotelegramid'
SCOPE_QLOCATION = 'qlocation'

URL_PROFILE = 'api/v1/profile'
