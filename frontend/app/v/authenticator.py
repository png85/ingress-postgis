
from hashlib import md5
import requests
import time

from app import v


class Authenticator:
    client_id = None
    client_secret = None
    redirect_url = None
    scopes = None
    state = None
    code = None
    access_token = None
    refresh_token = None
    expires_in = None
    expires_at = None

    ROOT = 'https://v.enl.one/oauth/'

    def get_auth_url(self):
        for param in [self.redirect_url, self.scopes, self.client_id, self.client_secret, self.state]:
            if not param:
                raise Exception('Missing mandatory configuration entry')

        scope = ''
        if isinstance(self.scopes, list):
            scope = '%20'.join(self.scopes)
        else:
            scope = self.scopes

        url = '{}{}?type=web_server&client_id={}&redirect_uri={}&response_type=code&scope={}&state={}' \
            .format(self.ROOT,
                    v.ENDPOINT_AUTH,
                    self.client_id,
                    self.redirect_url,
                    scope,
                    md5(self.state).hexdigest())

        return url

    def get_token(self, state):
        if not self.code:
            raise Exception('Missing code.')

        post_fields = {
            'grant_type': 'authorization_code',
            'code': self.code,
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'redirect_uri': self.redirect_url,
            'state': state
        }
        result = self.execute_api_post(v.ENDPOINT_TOKEN, post_fields)

        for k in ['access_token', 'refresh_token', 'expires_in']:
            if not k in result:
                raise Exception(
                    'Missing key "{}" in V access token!'.format(k))

        self.access_token = result['access_token']
        self.refresh_token = result['refresh_token']
        self.expires_in = result['expires_in']
        self.expires_at = time.time() + result['expires_in']

        return result['access_token']

    def get_v_info(self, fields={}):
        if not self.access_token:
            raise Exception('Missing access token!')

        result = self.execute_api_post(v.URL_PROFILE, fields, {
            'Authorization': 'Bearer {}'.format(self.access_token)
        })

        if 'error' in result:
            raise Exception('V API error "{}" when requesting V info: {}'.format(result['error'],
                                                                                 result['error_description']))

        return result['data']

    def set_redirect_url(self, url):
        self.redirect_url = url

    def set_scopes(self, scopes):
        self.scopes = scopes

    def set_code(self, code):
        self.code = code

    def execute_api_post(self, endpoint, fields, headers={}):
        """Execute POST API request

        This calls the requested API endpoint with a POST request and returns the returned JSON
        data as a Python object.

        Args:
            endpoint (string): API endpoint to call
            fields (dict): Dictionary of form fields to POST to API endpoint

        Returns:
            object: Python object generated from API JSON response
        """
        request_url = '{}{}'.format(self.ROOT, endpoint)
        response = requests.post(request_url, fields, headers=headers)

        try:
            result = response.json()
            if 'error' in result:
                raise Exception('Got error code "{}" from V: {}'.format(
                    result['error'], result['error_description']))

            return result

        except ValueError as ex:
            raise Exception(
                'Failed to decode JSON response from V API: {}'.format(str(ex)))
