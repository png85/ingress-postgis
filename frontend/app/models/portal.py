from geoalchemy2.elements import WKBElement
from geoalchemy2.shape import to_shape
from geoalchemy2.types import Geometry
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import DateTime, Integer, Text
from sqlalchemy_serializer.serializer import SerializerMixin
from app import app

Base = app.DB.Model


class Portal(Base, SerializerMixin):
    """Record for a single Ingress portal"""
    __tablename__ = 'portal'

    serialize_types = (
        (WKBElement, lambda x: to_shape(x).to_wkt()),
    )

    id = Column(Integer, primary_key=True)

    ingress_guid = Column(Text, nullable=False, unique=True)
    intel_link = Column(Text, nullable=False)
    title = Column(Text, nullable=False)
    image = Column(Text)
    location = Column(Geometry(geometry_type='POINT', srid=4326))
    address = Column(Text, nullable=True)
    address_last_query = Column(DateTime, nullable=True)

    def __repr__(self) -> str:
        return f'<Portal #{self.id}: "{self.title}">'
