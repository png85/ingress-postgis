from .portal import Portal
from .fieldlayer import FieldLayer
from .field import Field

import sqlalchemy
from sqlalchemy.sql.expression import select
from sqlalchemy.sql.operators import or_
from sqlalchemy.orm import column_property

#
# Add subquery showing usage as layer to Portal
Portal.usage_layer = column_property(
    select(sqlalchemy.func.count(FieldLayer.id)).where(
        FieldLayer.guid == Portal.ingress_guid)
    .scalar_subquery()
)

#
# Add subquery showing usage as anchor to Portal
Portal.usage_anchor = column_property(
    select(sqlalchemy.func.count(Field.id))
    .where(or_(Field.anchor_a_guid == Portal.ingress_guid,
               Field.anchor_b_guid == Portal.ingress_guid))
    .scalar_subquery()
)
