from app import app
from sqlalchemy_serializer import SerializerMixin
from geoalchemy2.elements import WKBElement
from geoalchemy2.shape import to_shape
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import BigInteger, Integer, Text
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
import shapely

Base = app.DB.Model


class FieldLayer(Base, SerializerMixin):
    """Layer in a Field"""
    __tablename__ = 'field_layer'

    serialize_rules = (
        ('-field.layers'),
        ('polygon'),
        ('area'),
        ('-address_last_query'),
    )

    serialize_types = (
        (WKBElement, lambda x: to_shape(x).to_wkt()),
    )

    id = Column(Integer, primary_key=True)

    # Field that this layer belongs to
    field_id = Column(Integer, ForeignKey('field.id'), nullable=False)
    field = relationship('Field', foreign_keys='FieldLayer.field_id')

    # Portal used to link to anchors
    guid = Column(Text, ForeignKey('portal.ingress_guid'), nullable=False)
    portal = relationship('Portal', foreign_keys='FieldLayer.guid')

    order = Column(Integer, nullable=False, default=0)

    def _shapelyPolygon(self):
        def _mapCoords(portal):
            shape = to_shape(portal.location)
            return shapely.geometry.mapping(shape)['coordinates']

        A = _mapCoords(self.field.anchor_a)
        B = _mapCoords(self.field.anchor_b)
        L = _mapCoords(self.portal)

        poly = shapely.geometry.Polygon([A, L, B, A])

        return poly

    @hybrid_property
    def polygon(self):
        """Generate shapely.Polygon object for this layer.

        Returns:
            shapely.Polygon: A shapely Polygon describing this layer
        """
        return self._shapelyPolygon().to_wkt()

    @hybrid_property
    def area(self):
        """Get area in square kilometers

        Returns:
            float: Area covered by this layer in square kilometers
        """
        return self._shapelyPolygon().area * 6378.137

    mind_units = Column(BigInteger, nullable=True, default=0)

    def __repr__(self) -> str:
        return f'<FieldLayer #{self.id}: Field #{self.field_id}/{self.guid}>'
