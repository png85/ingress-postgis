from geoalchemy2.shape import to_shape
from sqlalchemy.sql.expression import select
from app import app
from sqlalchemy_serializer import SerializerMixin
from geoalchemy2.elements import WKBElement
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import Integer, Text
from sqlalchemy.orm import relationship, column_property
import sqlalchemy

from .fieldlayer import FieldLayer

Base = app.DB.Model


class Field(Base, SerializerMixin):
    """Field consisting of two anchor Portals and multiple FieldLayers."""
    __tablename__ = 'field'

    serialize_types = (
        (WKBElement, lambda x: to_shape(x).to_wkt()),
    )

    serialize_rules = (
        ('-layers.field'),
    )

    id = Column(Integer, primary_key=True)

    title = Column(Text, nullable=False)

    # First anchor portal
    anchor_a_guid = Column(Text, ForeignKey(
        'portal.ingress_guid'), nullable=False)
    anchor_a = relationship('Portal', foreign_keys='Field.anchor_a_guid')

    # Second anchor portal
    anchor_b_guid = Column(Text, ForeignKey(
        'portal.ingress_guid'), nullable=False)
    anchor_b = relationship('Portal', foreign_keys='Field.anchor_b_guid')

    # Layers belonging to this field
    layers = relationship('FieldLayer', viewonly=True)

    maxOrder = column_property(
        select(sqlalchemy.func.max(FieldLayer.order)).where(
            FieldLayer.field_id == id).scalar_subquery()
    )

    def __repr__(self) -> str:
        return f'<Field #{self.id}: ({self.anchor_a_guid}, {self.anchor_b_guid})>'
