from . decorators import roles_required
from . import models

from . import cli
from . import views

from flask import g
from app import app


def username_exists(username):
    return app.DB.session.query(sqlalchemy.func.count(User.id)) \
        .where(User.username == username).scalar() > 0


@app.template_global('rbac_has_role')
def user_has_role(role_name):
    """Check if current user has given role

    This can be used in templates to check whether the current user has a certain role.

    Args:
        role_name (string): Name of the role to check

    Returns:
        bool: True if the current user has the given role
    """
    if g.current_user is not None:
        for role in g.current_user['roles']:
            if role['name'] == role_name:
                return True

    return False


@app.template_global('rbac_has_roles')
def user_has_roles(role_names):
    """Check if current use has a set of roles

    This can be used in templates to check whether the current user has a set of roles.

    Args:
        role_names (list): List of role names to check

    Returns:
        bool: True if the current user has all of the given roles
    """
    if g.current_user is not None:
        def _check(role):
            for r in g.current_user['roles']:
                if r['name'] == role:
                    return True
            return False

        results = list(map(_check, role_names))
        if not False in results:
            return True

    return False
