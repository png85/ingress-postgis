from app import app
from app.rbac.models import User, Role
from flask.cli import AppGroup, with_appcontext
import click
import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound

user_cli = AppGroup('rbac.users', help='Manage RBAC users.')


@user_cli.command('grant-role', with_appcontext=True, help='Grant role to user')
@click.argument('user')
@click.argument('role')
def grant_role(user, role):
    try:
        with app.DB.session.begin() as tx:
            user_data = None
            try:
                user_data = User.query.where(User.username == user).one()
            except NoResultFound as e:
                app.logger.error('User not found: %s', user)
                return

            role_data = None
            try:
                role_data = Role.query.where(Role.name == role).one()
            except NoResultFound as e:
                app.logger.error('Role not found: %s', role)
                return

            for r in user_data.roles:
                if r.id == role_data.id:
                    app.logger.info('User "%s" already has role "%s"',
                                    user, role)
                    return

            user_data.roles.append(role_data)

    except Exception as e:
        app.logger.error(
            'Caught exception while trying to grant role: %s', str(e))


@user_cli.command('revoke-role')
@click.argument('user')
@click.argument('role')
def revoke_role(user, role):
    try:
        with app.DB.session.begin() as tx:
            user_data = None
            try:
                user_data = User.query.where(User.username == user).one()
            except NoResultFound as e:
                app.logger.error('User not found: %s', user)
                return

            role_data = None
            try:
                role_data = Role.query.where(Role.name == role).one()
            except NoResultFound as e:
                app.logger.error('Role not found: %s', role)
                return

            user_data.roles.remove(role_data)

    except Exception as e:
        app.logger.error(
            'Caught exception while trying to revoke role: %s', str(e))


app.cli.add_command(user_cli)
