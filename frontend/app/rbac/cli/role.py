from app import app
from app.rbac.models import Role
from flask.cli import AppGroup, with_appcontext
import click
import sqlalchemy


role_cli = AppGroup('rbac.roles', help='Manage RBAC roles.')


@role_cli.command('create', with_appcontext=True, help='Create new role')
@click.argument('name')
@click.option('--grantable', default=True, help='Is role grantable?')
@click.option('--description', default=None, help='Description for new role')
def create(name, grantable=False, description=None):
    try:
        with app.DB.session.begin() as tx:
            if app.DB.session.query(sqlalchemy.func.count(Role.id)) \
                    .where(Role.name == name).scalar() > 0:
                app.logger.error('Role "%s" already exists!', name)
                return

            role = Role(name=name,
                        description=description,
                        grantable=grantable
                        )
            app.DB.session.add(role)
            app.DB.session.flush()

            app.DB.session.refresh(role)
            app.logger.info('Created new role: %s (ID: %d)', name, role.id)

    except Exception as e:
        app.logger.error('Caught exception while trying to create new Role: %s',
                         str(e))


@role_cli.command('delete', with_appcontext=True, help='Delete role from database')
@click.argument('name')
def delete(name):
    try:
        with app.DB.session.begin() as tx:
            role = Role.query.where(Role.name == name)
            if role.count() == 0:
                app.logger.error('Role "%s" not found!', name)
                return

            role.delete()

    except Exception as e:
        app.logger.error('Exception while removing role: %s', str(e))


INITIAL_ROLES = {
    'Site Admin': {
        'grantable': True,
        'description': 'Global site administrator',
    },
    'User Manager': {
        'grantable': True,
        'description': 'Manage users'
    },
    'Role Manager': {
        'grantable': True,
        'description': 'Manage roles'
    }
}


@role_cli.command('init', with_appcontext=True, help='Create initial roles for web interface')
def init():
    try:
        with app.DB.session.begin() as tx:
            for (role, properties) in INITIAL_ROLES.items():
                if Role.query.where(Role.name == role).count() == 0:
                    r = Role(name=role,
                             grantable=properties['grantable'],
                             description=properties['description'])
                    app.DB.session.add(r)
                    app.logger.info('Added role: %s', role)

    except Exception as e:
        app.logger.error(
            'Caught exception during role initialization: %s', str(e))


app.cli.add_command(role_cli)
