from flask import Blueprint

from . import user
from . import role

bp = Blueprint('rbac', __name__, url_prefix='/rbac')

bp.register_blueprint(user.bp)
bp.register_blueprint(role.bp)
