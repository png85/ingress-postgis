from app import app
from app.auth.decorators import login_required
from app.rbac.decorators import roles_required
from app.rbac.models import User, Role
from flask import Blueprint, jsonify, render_template, g, request
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.security import check_password_hash, generate_password_hash

bp = Blueprint('user', __name__, url_prefix='/user')
ajax_bp = Blueprint('ajax', __name__, url_prefix='/ajax')


@bp.route('/my_profile.html')
@login_required
def my_profile():
    return render_template('rbac/user/my_profile.html')


@bp.route('/<int:user_id>/profile.html')
@login_required
def profile(user_id):
    return 'TODO'


@bp.route('/list.html')
@roles_required(['User Manager'])
def list():
    return render_template('rbac/user/list.html')


@ajax_bp.route('/list.json')
@roles_required(['User Manager'])
def ajax_list():
    try:
        with app.DB.session.begin() as tx:
            users = [u for u in map(lambda x: x.to_dict(), User.query.all())]
            return jsonify(users)

    except Exception as e:
        app.logger.error('Caught exception while trying to load user list: %s',
                         str(e))
        return jsonify([])


@ajax_bp.route('/<int:user_id>/toggle_banned', methods=['GET'])
@roles_required(['User Manager'])
def toggle_banned(user_id):
    if user_id == g.current_user['id']:
        return jsonify({'success': False,
                        'message': 'You can not ban your own account.'})

    try:
        with app.DB.session.begin() as tx:
            user = User.query.where(User.id == user_id).one()

            def _has_role(role_name):
                for r in user.roles:
                    if r.name == role_name:
                        return True
                return False

            if True in [_has_role(x) for x in ['Site Admin']]:
                return jsonify({'success': False,
                                'message': f'User "{user.username}" has one or more protected role(s)'})

            user.banned = (not user.banned)

            return jsonify({'success': True,
                            'banned': user.banned})

    except NoResultFound as e:
        return jsonify({'success': False,
                        'message': 'Invalid user ID'})


@ajax_bp.route('/<int:user_id>/toggle_password_login', methods=['GET'])
@roles_required(['User Manager'])
def toggle_password_login(user_id):
    try:
        with app.DB.session.begin() as tx:
            user = None
            try:
                user = User.query.where(User.id == user_id).one()
            except NoResultFound as e:
                return jsonify({'success': False,
                                'message': 'Invalid User ID'})

            user.allow_password_login = (not user.allow_password_login)
            app.DB.session.flush()
            app.DB.session.refresh(user)

            return jsonify({'success': True,
                            'allow_password_login': user.allow_password_login})

    except Exception as e:
        return jsonify({'success': False,
                        'message': f'Caught exception: {str(e)}'})


@ajax_bp.route('/<int:user_id>/roles/<int:role_id>/revoke', methods=['GET'])
@roles_required(['User Manager'])
def revoke_role(user_id, role_id):
    try:
        with app.DB.session.begin() as tx:
            user = None
            try:
                user = User.query.where(User.id == user_id).one()
            except NoResultFound as e:
                return jsonify({'success': False,
                                'message': 'Invalid User ID'})

            role = None
            try:
                role = Role.query.where(Role.id == role_id).one()
            except NoResultFound as e:
                return jsonify({'success': False,
                                'message': 'Invalid Role ID'})

            if not role in user.roles:
                return jsonify({'success': False,
                                'message': f'User "{user.username}" does not have role "{role.name}"'})

            user.roles.remove(role)
            app.DB.session.flush()
            app.DB.session.refresh(user)

            return jsonify({'success': True,
                            'roles': [r for r in map(lambda x: x.to_dict(only=('id',
                                                                               'name',
                                                                               'description',
                                                                               'grantable')), user.roles)]})

    except Exception as e:
        app.logger.error(
            'Caught exception while trying to revoke role: %s', str(e))
        return jsonify({
            'success': False,
            'message': 'Caught exception while trying to revoke role!'
        })


@ajax_bp.route('/<int:user_id>/roles/<int:role_id>/grant', methods=['GET'])
@roles_required(['User Manager'])
def grant_role(user_id, role_id):

    try:
        with app.DB.session.begin() as tx:
            user = None
            try:
                user = User.query.where(User.id == user_id).one()
            except NoResultFound as e:
                return jsonify({'success': False,
                                'message': 'Invalid User ID'})

            role = None
            try:
                role = Role.query.where(Role.id == role_id).one()
            except NoResultFound as e:
                return jsonify({'success': False,
                                'message': 'Invalid Role ID'})

            if role in user.roles:
                return jsonify({'success': False,
                                'message': f'User <em>{user.username}</em> already has role <em>{role.name}</em>'})

            user.roles.append(role)
            app.DB.session.flush()
            app.DB.session.refresh(user)

            return jsonify({'success': True,
                            'roles': [r for r in map(lambda x: x.to_dict(only=('id',
                                                                               'name',
                                                                               'description',
                                                                               'grantable')), user.roles)]})

    except Exception as e:
        app.logger.error('Exception while granting role: %s', str(e))
        return jsonify({'success': False,
                        'message': f'Caught exception while trying to grant role: {str(e)}'})


@ajax_bp.route('/change_password', methods=['POST'])
@login_required
def change_password():
    # Make sure user is allowed to have a password
    if not g.current_user['allow_password_login']:
        return jsonify({'success': False,
                        'message': f"User {g.current_user['username']} is not allowed to login using password."})

    # Make sure all required form arguments are present
    mandatory_args = [('pw_1st', 'First password field'),
                      ('pw_2nd', 'Second password field')]
    if g.current_user['has_password_hash']:
        mandatory_args.append(('pw_old', 'Previous password'))

    for k, v in mandatory_args:
        if not k in request.form or len(request.form[k]) == 0:
            return jsonify({'success': False,
                           'message': f'Missing mandatory form argument: {v}'})

    # Compare supplied new passwords
    if request.form['pw_1st'] != request.form['pw_2nd']:
        return jsonify({'success': False,
                        'message': 'Supplied passwords are not equal.'})

    try:
        with app.DB.session.begin() as tx:
            user_data = User.query.where(User.id == g.current_user['id']).one()

            # Check existing password
            if user_data['password_hash'] != None:
                if not check_password_hash(user_data.password_hash, request.form['pw_1st']):
                    return jsonify({'success': False,
                                    'message': f'Incorrect old password supplied.'})

            # Make sure new password matches password policy
            from app.auth import password_policy
            p = password_policy.password(request.form['pw_1st'])
            failed_tests = [t.name() for t in p.test()]

            if len(failed_tests) > 0:
                test_names = ', '.join(failed_tests)
                return jsonify({'success': False,
                                'message': f'Password failed the following strength tests: {test_names}',
                                'test_names': test_names})

            # Update password hash in DB
            new_hash = generate_password_hash(request.form['pw_1st'])
            user_data.password_hash = new_hash

            return jsonify({'success': True,
                            'message': 'Password updated successfully.'})

    except Exception as e:
        return jsonify({'success': False,
                        'message': f'Caught exception: {str(e)}'})


bp.register_blueprint(ajax_bp)
