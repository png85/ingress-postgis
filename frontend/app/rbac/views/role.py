from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import and_
from app import app
from app.rbac.models import Role, User
from app.auth.decorators import login_required
from app.rbac.decorators import roles_required
from flask import (Blueprint, render_template, jsonify, request, flash,
                   redirect, url_for)

bp = Blueprint('role', __name__, url_prefix='/role')
ajax_bp = Blueprint('ajax', __name__, url_prefix='/ajax')


@bp.route('/list.html', methods=['GET'])
@roles_required(['Role Manager'])
def list():
    return render_template('rbac/role/list.html')


@bp.route('/create.html', methods=['POST'])
@roles_required(['Role Manager'])
def create():
    REDIRECT_URL = url_for('rbac.role.list')

    for k in ['name', 'description', 'grantable']:
        if not k in request.form:
            flash(f'Missing form parameter: <em>{k}</em>', 'danger')
            return redirect(REDIRECT_URL)

    if not request.form['grantable'] in ['True', 'False']:
        flash(f"Invalid value for <em>grantable</em> flag: <em>{request.form['grantable']}",
              'danger')
        return redirect(REDIRECT_URL)

    role_name = request.form['name']
    grantable = (request.form['grantable'] == 'True')
    description = None

    if len(request.form['description']) > 0:
        description = request.form['description']

    try:
        with app.DB.session.begin() as tx:
            if Role.query.where(Role.name == role_name).count() > 0:
                flash(f'A role named <em>{role_name}</em> already exists',
                      'danger')
                return redirect(REDIRECT_URL)

            role = Role(name=role_name,
                        grantable=grantable,
                        description=description)

            app.DB.session.add(role)

            flash(f'Role <em>{role_name}</em> created successfully.',
                  'success')

        return redirect(REDIRECT_URL)

    except Exception as e:
        flash(f'Caught exception while trying to create new role: {str(e)}',
              'danger')
        return redirect(REDIRECT_URL)


@bp.route('/<int:role_id>/delete.html', methods=['POST'])
@roles_required(['Role Manager'])
def delete(role_id):
    REDIRECT_URL = url_for('rbac.role.list')
    try:
        with app.DB.session.begin() as tx:
            role = Role.query.where(Role.id == role_id)
            if role.count() == 0:
                flash(f'Invalid Role ID', 'danger')
                return redirect(REDIRECT_URL)

            role.delete()
            flash('Role deleted successfully.', 'success')

            return redirect(REDIRECT_URL)

    except Exception as e:
        flash(f'Caught exception while trying to delete role: {str(e)}',
              'danger')
        return redirect(REDIRECT_URL)


@ajax_bp.route('/list.json', methods=['GET'])
@roles_required(['Role Manager'])
def ajax_list():
    try:
        with app.DB.session.begin() as tx:
            roles = [r for r in map(lambda x: x.to_dict(only=('id',
                                                              'name', 'grantable', 'description',
                                                              'users.id', 'users.username')),
                                    Role.query.all())]
            return jsonify(roles)

    except Exception as e:
        app.logger.error('Caught exception while loading role list: %s',
                         str(e))
        return jsonify([])


@ajax_bp.route('/<int:role_id>/toggle_grantable')
@roles_required(['Role Manager'])
def toggle_grantable(role_id):
    try:
        with app.DB.session.begin() as tx:
            role = None
            try:
                role = Role.query.where(Role.id == role_id).one()

            except NoResultFound as e:
                return jsonify({'success': False,
                                'message': 'Invalid Role ID!'})

            role.grantable = (not role.grantable)

            return jsonify({'success': True,
                            'grantable': role.grantable})

    except Exception as e:
        return jsonify({'success': False,
                        'message': 'Caught exception while trying to toggle grantable flag.'})


@ajax_bp.route('/grantable_roles.json', methods=['POST'])
@login_required
def grantable_role_search():
    for k in ['q', 'uid']:
        if not k in request.form or len(request.form[k]) == 0:
            return jsonify([])

    if not request.form['uid'].isnumeric():
        return jsonify([])

    try:
        with app.DB.session.begin() as tx:
            q = request.form['q']
            user = None
            try:
                user = User.query.where(User.id == request.form['uid']).one()
            except NoResultFound as e:
                return jsonify([])

            # Select grantable roles by name
            roles = Role.query.where(and_(Role.grantable == True,
                                          Role.name.like(f'%{q}%'))) \
                .order_by(Role.name.asc()) \
                .all()

            # Filter roles the target user already has
            itFiltered = filter(lambda r: not (r in user.roles),
                                roles)

            # Serialize id/name columns to dictionaries
            itSerialized = map(lambda r: r.to_dict(only=('id',
                                                         'name')),
                               itFiltered)

            return jsonify([r for r in itSerialized])

    except Exception as e:
        app.logger.error('Caught exception during AJAX role search: %s',
                         str(e))
        return jsonify([])


bp.register_blueprint(ajax_bp)
