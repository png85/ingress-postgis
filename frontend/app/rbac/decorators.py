from functools import wraps
from flask import g, request
from flask.helpers import flash, url_for
from werkzeug.utils import redirect
from deprecated import deprecated


def roles_required(role_names=[]):
    """Wrapper for views that require a set of rules on a logged-in User.

    Prevents access to the wrapped view if the user is not logged in or doesn't have
    all the specified roles.
    """
    def wrapper(view):
        @wraps(view)
        def wrapped(*args, **kwargs):
            if g.current_user is None:
                flash(f'Access denied. You need to be logged in.', 'danger')
                return redirect(url_for('auth.login.login'))

            if not role_names:
                return view(*args, **kwargs)

            def is_missing(name):
                for r in g.current_user['roles']:
                    if r['name'] == name:
                        return False
                return True

            roles_missing = [r for r in filter(is_missing, role_names)]
            if len(roles_missing) > 0:
                flash('Access denied. Account is missing role(s): {}'.format(', '.join(roles_missing)),
                      'danger')
                return redirect(request.url_root)

            return view(*args, **kwargs)

        return wrapped

    return wrapper
