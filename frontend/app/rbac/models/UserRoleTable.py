from sqlalchemy.sql.schema import Column, ForeignKey, Table
from app import app

Base = app.DB.Model

#
# Association table for User/Role relationship
UserRoleTable = Table('rbac_user_roles',
                      Base.metadata,
                      Column('user_id',
                             ForeignKey('rbac_user.id',
                                        ondelete='CASCADE'),
                             primary_key=True),
                      Column('role_id',
                             ForeignKey('rbac_role.id',
                                        ondelete='CASCADE'),
                             primary_key=True)
                      )
Base = app.DB.Model
