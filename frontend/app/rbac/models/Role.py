from app import app
from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Boolean, Integer, Text
from sqlalchemy.orm import relationship

from . UserRoleTable import UserRoleTable

Base = app.DB.Model


class Role(Base, SerializerMixin):
    """Role that can be added to an User.
    """
    __tablename__ = 'rbac_role'

    serialize_rules = (
        ('-users.roles'),
    )

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False, unique=True)
    description = Column(Text, nullable=True)
    grantable = Column(Boolean, nullable=False, default=True)

    users = relationship('User',
                         secondary=UserRoleTable,
                         back_populates='roles')
