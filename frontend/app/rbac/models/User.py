from app import app

from sqlalchemy_serializer import SerializerMixin
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Boolean, DateTime, Integer, SmallInteger, Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

from . UserRoleTable import UserRoleTable
from . Role import Role


Base = app.DB.Model


class User(Base, SerializerMixin):
    """Record for an user that can log in to the site.
    """
    __tablename__ = 'rbac_user'

    serialize_rules = (
        ('-password_hash'), ('has_password_hash'),
        ('-roles.users'),
    )

    id = Column(Integer, primary_key=True)
    username = Column(Text, nullable=False, unique=True)
    last_login = Column(DateTime, nullable=True)

    #
    # Password login info
    password_hash = Column(Text, nullable=True)
    allow_password_login = Column(Boolean, nullable=False, default=True)

    @hybrid_property
    def has_password_hash(self):
        return self.password_hash != None

    #
    # Account state & roles
    banned = Column(Boolean, nullable=False, default=False)
    roles = relationship(Role,
                         secondary=UserRoleTable,
                         back_populates='users')

    #
    # V Profile data
    v_enlid = Column(Text, nullable=True)
    v_verified = Column(Boolean, nullable=True, default=True)
    v_points = Column(Integer, nullable=True)
    v_level = Column(SmallInteger, nullable=True)
