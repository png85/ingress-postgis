import datetime
from json.decoder import JSONDecodeError
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
from sqlalchemy import and_, or_
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError

from geoalchemy2.shape import to_shape

from shapely.geometry import mapping

from flask import (jsonify, abort, request, Blueprint, make_response, render_template,
                   url_for, redirect, flash)
from flask_cors import cross_origin

import json

from app import app
from app.models import Portal, Field, FieldLayer

from geopy import Nominatim

#
# Blueprints for URL matching
bp_rest = Blueprint('portals-rest', __name__, url_prefix='/api/portals')
bp = Blueprint('portals', __name__, url_prefix='/portals')


@bp_rest.route('/', methods=['GET'])
@cross_origin()
def rest_list_portals():
    portals = Portal.query.all()
    portals_serialized = list(map(lambda p: p.to_dict(), portals))

    return jsonify(portals_serialized)


def _deletePortal(guid):
    """Remove Portal from database

    Removes the portal with the given GUID including all Fields and Layers using this portal.

    Args:
        guid (string): Ingress GUID or the portal to remove

    Returns:
        bool: Boolean indicating whether removal was successful
    """
    try:
        with app.DB.session.begin() as tx:
            # Fetch Portal object to see if it's a valid GUID
            portalQuery = Portal.query.where(Portal.ingress_guid == guid)
            portal = portalQuery.one()

            # Delete all layers using this Portal
            layersUsingPortal = FieldLayer.query.where(FieldLayer.guid == guid)
            layersUsingPortal.delete()

            # Delete all fields using this Portal as an anchor
            fieldsUsingPortal = Field.query.where(or_(Field.anchor_a_guid == guid,
                                                      Field.anchor_b_guid == guid))
            for f in fieldsUsingPortal.all():
                layers = FieldLayer.query.where(
                    FieldLayer.field_id == Field.id)
                layers.delete()

            # Finally, remove Field records and the Portal itself
            fieldsUsingPortal.delete()
            portalQuery.delete()

    except NoResultFound as e:
        app.logger.error('Tried to delete portal with non-existant GUID: %s',
                         guid)
        return False

    except IntegrityError as e:
        app.logger.error('IntegrityError while deleting portal %s: %s',
                         guid, e.orig)
        return False

    return True


def _updatePortalFromJSON(guid, portal_json):
    for (k, check_null) in []:
        if not k in portal_json:
            return (False, False, f'Missing key in Portal JSON data: {k}')

        if check_null and not portal_json[k]:
            return (False, False, f'Value for JSON key "{k} can not be null')

    try:
        with app.DB.session.begin() as tx:
            newPortal = False
            try:
                portalQuery = Portal.query.where(Portal.ingress_guid == guid)
                portal = portalQuery.one()
            except NoResultFound as e:
                portal = Portal(ingress_guid=guid)
                newPortal = True

            if portal_json['image']:
                portal.image = portal_json['image']

            portal.title = portal_json['title']

            lat = portal_json['latE6'] / (10.0**6.0)
            lng = portal_json['lngE6'] / (10.0**6.0)
            portal.intel_link = f'https://intel.ingress.com/?pll={lat:.6f},{lng:.6f}'
            portal.location = f'POINT({lng:.6f} {lat:.6f})'

            if newPortal:
                app.DB.session.add(portal)

            return (True, newPortal, 'Portal data stored successfully.')

    except Exception as e:
        app.logger.error('Caught Exception while updating portal with GUID=%s: %s',
                         guid, str(e))
        return (False, False, 'Database error.')


@bp_rest.route('/bulk', methods=['PUT'])
@cross_origin()
def rest_bulk_update():
    if request.method == 'PUT':
        try:
            portals = json.loads(request.data)
            updateResults = {}
            guids = {
                'updated': [],
                'failed': []
            }

            for guid in portals:
                (successful,
                 isNewPortal,
                 message) = _updatePortalFromJSON(guid, portals[guid])

                if not successful:
                    updateResults[guid] = {'success': False,
                                           'message': message}
                    guids['failed'].append(guid)
                else:
                    updateResults[guid] = {'success': True,
                                           'is_new': isNewPortal,
                                           'message': message}
                    guids['updated'].append(guid)

            return jsonify({
                'guids': guids,
                'results': updateResults
            })

        except JSONDecodeError as e:
            app.logger.error(
                'Failed to parse bulk portal data: %s', request.data)
            return jsonify({
                'success': False,
                'message': 'Failed to parse portal data.',
                'raw_data': str(request.data)
            })

    return jsonify({
        'success': False,
        'message': 'Unsupported request method.'
    })


@bp_rest.route('/<string:guid>', methods=['GET', 'PUT', 'DELETE'])
@cross_origin()
def rest_portal_put(guid):
    #
    # HTTP PUT: Create or update portal record
    if request.method == 'PUT':
        try:
            #
            # Make sure all required properties are present in passed JSON object
            portal_json = json.loads(request.data)
            (success,
             newPortal,
             message) = _updatePortalFromJSON(guid, portal_json)

            return jsonify({
                'success': success,
                'is_new': newPortal,
                'message': message,
                'guid': guid
            })

        except json.decoder.JSONDecodeError as e:
            app.logger.error('Failed to parse portal JSON data: %s',
                             request.data)
            return make_response(jsonify({
                'success': False,
                'message': f'Failed to parse JSON data: "{request.data}"'
            }), 400)

    #
    # HTTP DELETE: Remove portal record
    elif request.method == 'DELETE':
        if _deletePortal(guid):
            return jsonify({'success': True})
        else:
            return jsonify({'success': False, 'message': 'Failed to remove portal from database.'})

    #
    # Default: Return data for given ingress GUID as JSON
    try:
        portal = Portal.query.filter(Portal.ingress_guid == guid).one()
        return jsonify(portal.to_dict())

    except NoResultFound as e:
        abort(404)


@bp.route('/')
def index():
    portals = Portal.query.all()
    return render_template('portals/index.html',
                           current_page='index',
                           portals=portals
                           )


@bp.route('/<int:id>.html', methods=['GET'])
def portal_by_id(id):
    try:
        portal = Portal.query.where(Portal.id == id).one()
        location = mapping(to_shape(portal.location))
        return render_template('portals/details.html', portal=portal, location=location)

    except NoResultFound:
        response = make_response('Portal not found!', 404)
        abort(response)


@bp.route('/<string:guid>/delete', methods=['POST'])
def delete(guid):
    if _deletePortal(guid):
        flash('Portal removed from database.', 'success')
    else:
        flash('Failed to remove portal from database!', 'danger')

    return redirect(url_for('portals.index'))


@bp.route('<string:guid>/ajax/fields.json')
def ajax_fields_for_portal(guid):
    try:
        fields = Field.query.where(or_(Field.anchor_a_guid == guid,
                                       Field.anchor_b_guid == guid)).all()
        fields_serialized = list(map(lambda x: x.to_dict(), fields))

        return jsonify(fields_serialized)

    except Exception as e:
        app.logger.error('Caught exception: %s', str(e))
        return jsonify([])


@bp.route('<string:guid>/ajax/layers.json')
def ajax_layers_for_portal(guid):
    try:
        layers = FieldLayer.query.where(FieldLayer.guid == guid).all()
        layers_serialized = list(map(lambda x: x.to_dict(), layers))

        return jsonify(layers_serialized)

    except Exception as e:
        app.logger.error('Caught exception: %s', str(e))
        return jsonify([])


@bp.route('/stats.html', methods=['GET'])
def stats():
    stats = {
    }

    try:
        portalCountQuery = app.DB.session.query(
            sqlalchemy.func.count(Portal.id))
        stats['portalCount'] = portalCountQuery.scalar()
    except Exception as e:
        app.logger.error('Exception while counting portals: %s', str(e))
        stats['portalCount'] = 0

    try:
        unusedCountQuery = app.DB.session.query(
            sqlalchemy.func.count(Portal.id)).filter(and_(Portal.usage_anchor == 0,
                                                          Portal.usage_layer == 0))
        stats['unusedCount'] = unusedCountQuery.scalar()
    except Exception as e:
        app.logger.error('Excepting while counting unused portals: %s', str(e))
        stats['unusedCount'] = 0

    return render_template('portals/stats.html',
                           stats=stats)


@bp.route('/scheduler-stats.html', methods=['GET'])
def scheduler_stats():
    return render_template('portals/scheduler-stats.html')


@bp.route('/prune', methods=['GET'])
def prune():
    guids = []
    with app.DB.session.begin() as tx:
        for portal in Portal.query.where(and_(Portal.usage_anchor == 0,
                                              Portal.usage_layer == 0)).all():
            guids.append(portal.ingress_guid)

    removedCount = 0
    for guid in guids:
        if not _deletePortal(guid):
            flash(f'Failed to remove portal with GUID {guid}', 'danger')
        else:
            removedCount += 1

    if removedCount > 0:
        flash(
            f'Removed {removedCount} unused portals from database.', 'success')

    return redirect(url_for('portals.index'))


@app.scheduler.task('interval', id='reverse_geocode', seconds=2, misfire_grace_time=30)
def find_portal_address():
    try:
        db = SQLAlchemy(app)
        with db.session.begin() as tx:
            portal = Portal.query.where(Portal.address == None) \
                .order_by(Portal.address_last_query.asc(), Portal.id.asc()) \
                .limit(1).one()

            m = mapping(to_shape(portal.location))['coordinates']
            n = Nominatim(user_agent=f'Ingress-GIS@{app.scheduler.host_name}')

            q = f'{m[1]} {m[0]}'
            location = n.reverse(q)

            if location.address:
                portal.address = location.address
                app.logger.info('%s: %s', portal.ingress_guid, portal.address)
            else:
                portal.address_last_query = datetime.datetime.now()

            tx.commit()

    except NoResultFound as e:
        pass

    except Exception as e:
        app.logger.error('Error in reverse geocode: %s', str(e))


@bp.route('/ajax/portals_without_address.json')
def portals_without_address():
    try:
        with app.DB.session.begin() as tx:
            portals = Portal.query.where(Portal.address == None).count()
            return jsonify({
                'count': portals
            })

    except Exception as e:
        app.logger.error('Caught exception: %s', str(e))
