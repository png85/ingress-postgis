from flask import (Blueprint, render_template, request, abort, make_response,
                   jsonify, make_response, flash, redirect, url_for)
import shapely

import sqlalchemy
from sqlalchemy import and_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from geoalchemy2.shape import to_shape

from shapely.geometry import mapping

import geojson

from app import app
from app.models import Portal, Field, FieldLayer

bp = Blueprint('fields', __name__, url_prefix='/fields')


@bp.route('/')
def index():
    return render_template('fields/index.html', current_page='index')


@bp.route('/<int:id>.html', methods=['GET'])
def view(id):
    try:
        field = Field.query.where(Field.id == id).one()

    except NoResultFound as e:
        flash(f'Field #{id} not found.', 'danger')
        return redirect(url_for('fields.index'))

    return render_template('fields/view.html', field=field.to_dict())


@bp.route('/<int:id>.geojson', methods=['GET'])
def view_geojson(id):
    try:
        field = Field.query.where(Field.id == id).one()

        def portalFeature(portal, portalType):
            s = to_shape(portal.location)
            f = geojson.Feature(geometry=s, properties={
                'title': portal.title
            })
            return f

        def layerFeature(layer):
            g = shapely.wkt.loads(layer.polygon)
            f = geojson.Feature(geometry=g, properties={
                'order': layer.order,
                'title': layer.portal.title,
                'area': layer.area,
            })
            return f

        layerQuery = FieldLayer.query.where(FieldLayer.field_id == id) \
            .order_by(FieldLayer.order.asc())
        layers = layerQuery.all()

        portalFeatures = list(map(lambda x: portalFeature(x.portal, 'Layer'),
                                  layers))
        portalFeatures.append(portalFeature(field.anchor_a, 'Anchor'))
        portalFeatures.append(portalFeature(field.anchor_b, 'Anchor'))

        layerFeatures = list(map(layerFeature, layers))

        return geojson.FeatureCollection(portalFeatures+layerFeatures)

    except NoResultFound as e:
        abort(404)


@bp.route('/create', methods=['GET', 'POST'])
def create():
    if request.method == 'POST':
        with app.DB.session.begin() as tx:
            for k in ['anchor_a_guid', 'anchor_b_guid', 'title']:
                if not k in request.form or request.form[k] == '':
                    flash(f'Missing parameter: <em>{k}</em>', 'danger')
                    return redirect(url_for('fields.create'))

            guid_a = request.form['anchor_a_guid']
            guid_b = request.form['anchor_b_guid']

            if guid_a == guid_b:
                flash(
                    f'Portal Anchors cannot have the same GUID: <em>{guid_a}</em>', 'danger')
                return redirect(url_for('fields.create'))

            def _portal_from_guid(guid):
                try:
                    portal = Portal.query.where(
                        Portal.ingress_guid == guid).one()
                    return portal

                except sqlalchemy.orm.exc.NoResultFound as e:
                    flash(
                        f'Failed to get Portal object for GUID {guid}', 'danger')
                    return None

            title = request.form['title']
            portal_a = _portal_from_guid(guid_a)
            portal_b = _portal_from_guid(guid_b)

            if portal_a is None or portal_b is None:
                return redirect(url_for('fields.create'))

            try:
                field = Field(anchor_a_guid=guid_a,
                              anchor_b_guid=guid_b,
                              title=title)

                app.DB.session.add(field)

                flash(f'Field <em>{ title }</em> created.', 'success')
                return redirect(url_for('fields.index'))

            except Exception as e:
                flash(
                    f'Exception while creating field object! {str(e)}', 'danger')

    return render_template('fields/create.html', current_page='create')


@bp.route('/<int:id>/delete', methods=['POST'])
def delete(id):
    try:
        with app.DB.session.begin() as tx:
            field = Field.query.where(Field.id == id).one()
            title = field.title

            layers = FieldLayer.query.where(FieldLayer.field_id == id)
            layers.delete()

            app.DB.session.delete(field)

        flash(f'Field <em>{title}</em> deleted.', 'success')

    except IntegrityError as e:
        flash(f'Database error while deleting Field #{id}', 'danger')

    return redirect(url_for('fields.index'))


@bp.route('/<int:id>/update_title', methods=['POST'])
def update_title(id):
    if not 'title' in request.form:
        return jsonify({'success': False,
                        'message': 'Missing title parameter!'})

    try:
        with app.DB.session.begin() as tx:
            field = Field.query.where(Field.id == id).one()

            if field.title != request.form['title']:
                field.title = request.form['title']

        return jsonify({'success': True,
                        'title': request.form['title']})

    except NoResultFound as e:
        return jsonify({'success': False,
                        'message': f'SQL error: {e.orig}'})


@bp.route('/<int:id>/layers/add', methods=['POST'])
def add_layer(id):
    # Make sure all form parameters are present
    for k in ['layer_portal', 'layer_portal_guid', 'order']:
        if not k in request.form or request.form[k] is None:
            flash(f'Missing parameter {k} in form!', 'danger')
            return redirect(url_for('fields.view', id=id))

    guid = request.form['layer_portal_guid']

    try:
        with app.DB.session.begin() as tx:
            field = Field.query.where(Field.id == id).one()

            if field.anchor_a.ingress_guid == guid or field.anchor_b.ingress_guid == guid:
                flash(f'Layer portal can not be one of the anchors!', 'warning')
                return redirect(url_for('fields.view', id=id))

            layerCount = app.DB.session.query(sqlalchemy.func.count(FieldLayer.id)) \
                .where(and_(FieldLayer.field_id == field.id,
                            FieldLayer.guid == guid)) \
                .scalar()
            if layerCount > 0:
                flash(f'Layer already exists for field <em>{field.title}</em>')
                return redirect(url_for('fields.view', id=id))

            layer = FieldLayer(field_id=id,
                               guid=guid,
                               order=request.form['order'])

            app.DB.session.add(layer)
            flash(
                f'''Added layer <em>{request.form['layer_portal']}</em> to field.''', 'success')

    except NoResultFound as e:
        abort(404)

    return redirect(url_for('fields.view', id=id))


@bp.route('/<int:field_id>/layers/<int:layer_id>/delete', methods=['POST'])
def delete_layer(field_id, layer_id):
    try:
        with app.DB.session.begin() as tx:
            layerQuery = FieldLayer.query.where(FieldLayer.id == layer_id)
            layer = layerQuery.one()
            title = layer.portal.title

            layerQuery.delete()

        flash(f'Removed <em>{title}</em> from field.', 'success')

    except NoResultFound as e:
        flash('Failed to remove layer.', 'danger')

    return redirect(url_for('fields.view', id=field_id))


@bp.route('/ajax/search_portal', methods=['POST'])
def ajax_search_portal():
    if not 'q' in request.form or request.form['q'] is None:
        return '[]'

    searchString = request.form['q']

    portals = Portal.query.filter(Portal.title.like(
        f'%{searchString}%')).order_by(Portal.title.asc()).all()
    portals_serialized = []
    for portal in portals:
        portals_serialized.append({
            'guid': portal.ingress_guid,
            'title': portal.title,
            'image': portal.image
        })

    response = make_response(jsonify(portals_serialized))

    return response


@bp.route('/ajax/list_fields', methods=['GET'])
def ajax_list_fields():
    fields = Field.query.all()
    fields_serialized = list(map(lambda f: f.to_dict(), fields))
    return jsonify(fields_serialized)


@ bp.route('/ajax/<int:id>/layers', methods=['GET'])
def ajax_list_layers(id):
    layers = FieldLayer.query.where(FieldLayer.field_id == id).order_by(
        FieldLayer.order.asc()).all()
    layers_serialized = list(map(lambda x: x.to_dict(), layers))

    return jsonify(layers_serialized)


@bp.route('/ajax/<int:field_id>/<int:layer_id>/order_inc')
def ajax_layer_order_inc(field_id, layer_id):
    try:
        with app.DB.session.begin() as tx:
            layer = FieldLayer.query.where(and_(FieldLayer.id == layer_id,
                                                FieldLayer.field_id == field_id)).one()
            layer.order += 1

        return jsonify({'success': True,
                        'message': 'Layer order updated.'})

    except NoResultFound as e:
        return jsonify({'success': False,
                        'message': f'FieldLayer {field_id}/{layer_id} not found!'})


@bp.route('/ajax/<int:field_id>/<int:layer_id>/order_dec')
def ajax_layer_order_dec(field_id, layer_id):
    try:
        with app.DB.session.begin() as tx:
            layer = FieldLayer.query.where(and_(FieldLayer.id == layer_id,
                                                FieldLayer.field_id == field_id)).one()
            if layer.order < 1:
                return jsonify({'success': False,
                                'message': 'FieldLayer cannot have negative order!'})
            layer.order -= 1

        return jsonify({'success': True,
                        'message': 'Layer order updated.'})

    except NoResultFound as e:
        return jsonify({'success': False,
                        'message': f'FieldLayer {field_id}/{layer_id} not found!'})


@bp.route('/ajax/<int:id>/max_order', methods=['GET'])
def ajax_max_order(id):
    try:
        field = Field.query.where(Field.id == id).one()

        return jsonify({'success': True,
                        'max_order': field.maxOrder,
                        'field_id': id})

    except NoResultFound as e:
        return jsonify({'success': False,
                        'message': f'Field not found!'})


@bp.route('/ajax/<int:field_id>/<int:layer_id>/update_mu', methods=['POST'])
def ajax_update_layer_mu(field_id, layer_id):
    if not 'mind_units' in request.form:
        return jsonify({
            'success': False,
            'message': 'Missing form value(s)!'
        })

    MU = request.form['mind_units']
    if not MU.isnumeric():
        return jsonify({
            'success': False,
            'message': f'Invalid value for mind_units: {MU}'
        })

    try:
        with app.DB.session.begin() as tx:
            layer = FieldLayer.query.where(and_(FieldLayer.field_id == field_id,
                                                FieldLayer.id == layer_id)).one()

            if layer.mind_units != MU:
                layer.mind_units = MU
                return jsonify({
                    'success': True,
                    'updated': True,
                })

            return jsonify({
                'success': True,
                'updated': False
            })

    except NoResultFound as e:
        abort(404)

    return f'TODO ({field_id}:{layer_id})'
