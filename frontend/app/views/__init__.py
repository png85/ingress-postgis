from . import portals
from . import fields
from . import admin

from flask import render_template
from app import app


@app.route('/')
def index():
    return render_template('index.html')
