from app.rbac.decorators import roles_required
from flask import Blueprint, render_template

bp = Blueprint('admin', __name__, url_prefix='/admin')


@bp.route('/')
@roles_required(['Site Admin'])
def index():
    return render_template('admin/index.html')
