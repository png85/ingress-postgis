from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from geoalchemy2.shape import to_shape
from flask_debugtoolbar import DebugToolbarExtension
from flask_apscheduler import APScheduler
from flask_jsglue import JSGlue
from flask_minify import minify
import os
import logging

jsglue = JSGlue()


def create_app():
    a = Flask(__name__, instance_relative_config=True)

    if not os.path.exists(a.instance_path):
        try:
            os.makedirs(a.instance_path)
        except OSError as e:
            a.logger.error('Failed to create instance path "%s": %s',
                           a.instance_path, str(e))

    for subdir in ['cache']:
        path = os.path.join(a.instance_path, subdir)
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as e:
                a.logger.error('Failed to create directory "%s": %s',
                               path, str(e))
    a.debug = True

    a.config.from_pyfile('config.py')

    CORS(a)

    a.DB = SQLAlchemy(a)

    def is_debug_mode():
        """Get app debug status."""
        debug = os.environ.get("FLASK_DEBUG")
        if not debug:
            return os.environ.get("FLASK_ENV") == "development"
        return debug.lower() not in ("0", "false", "no")

    def is_werkzeug_reloader_process():
        """Get werkzeug status."""
        return os.environ.get("WERKZEUG_RUN_MAIN") == "true"

    a.migrate = Migrate(a, a.DB)
    a.toolbar = DebugToolbarExtension(a)
    a.scheduler = APScheduler()
    a.scheduler.init_app(a)
    jsglue.init_app(a)

    if 'MINIFY_ENABLED' in a.config and a.config['MINIFY_ENABLED'] is True:
        a.minify = minify(app=a)

    logging.getLogger("apscheduler").setLevel(logging.WARN)

    with a.app_context():
        if not is_debug_mode() and not is_werkzeug_reloader_process():
            pass
        else:
            a.scheduler.start()

    return a


app = create_app()

migrate = app.migrate

toolbar = app.toolbar

if True:
    from app import auth
    from app import rbac
    from app import models
    from app import views
    from app import v
    from app import diceware

app.register_blueprint(views.portals.bp_rest)
app.register_blueprint(views.portals.bp)
app.register_blueprint(views.fields.bp)
app.register_blueprint(v.views.bp)
app.register_blueprint(views.admin.bp)
app.register_blueprint(auth.views.bp)
app.register_blueprint(rbac.views.bp)


@app.template_filter('to_wkt')
def _to_wkt_template_filter(wkb):
    shapely_geometry = to_shape(wkb)
    return shapely_geometry.to_wkt()
