#/bin/sh

echo "-- Enabling Python virtualenv..."
. /usr/local/venv/bin/activate

echo "-- Launching Flask WSGI server..."
wait-for-it -t 60 -s db:5432 -- python main.py
