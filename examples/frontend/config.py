#
# Secret Key for session cookies
SECRET_KEY = 'super-secret-dev-key'

#
# Disable redirect interception in FDT
DEBUG_TB_INTERCEPT_REDIRECTS = False

#
# Enable scheduler API
SCHEDULER_API_ENABLED = True

#
# Database connection parameters
SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://ingress_gis:hackmenow@db:5432/ingress_gis'
SQLALCHEMY_TRACK_MODIFICATIONS = True

#
# Allow registration of new user accounts
RBAC_REGISTRATION_ALLOWED = True

#
# v.enl.one API credentials
V_API_CLIENT_ID = 'CHANGE_THIS'
V_API_CLIENT_SECRET = 'CHANGE_THIS'

#
# V flag requirements
V_PROFILE_REQUIRED_FLAGS = {
    'quarantine': (False, 'Your V account has been quarantined.'),
    'blacklisted': (False, 'Your V account has been blacklisted.'),
    'verified': (True, 'Your V account is not verified.'),
    'flagged': (False, 'Your V account is flagged.'),
    'banned_by_nia': (False, 'Your Ingress account has been banned.')
}

#
# Minify all HTML/CSS/JS output
MINIFY_ENABLED = True
